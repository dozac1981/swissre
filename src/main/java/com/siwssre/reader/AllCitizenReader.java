package com.siwssre.reader;


import com.siwssre.model.Citizen;
import com.siwssre.util.Utils;

import java.io.BufferedReader;
import java.util.List;
import java.util.stream.Collectors;

public class AllCitizenReader implements BaseReader<BufferedReader, List<Citizen>> {

    /**
     * Create a Citizen object based on a String[] inp
     * @param metadata
     * @return
     */
    private static Citizen createWallet(String[] metadata) {
        String lastName = metadata[0];
        String firstName = metadata[1];
        String dob = metadata[2];
        String email = metadata[3];

        return new Citizen(lastName, firstName, Utils.formatDob(dob), email);
    }

    /**
     * File reader will return a list of Citizens based on the file input as BufferedReader
     * @param br
     * @return
     */
    @Override
    public List<Citizen> read(BufferedReader br) {
        List<Citizen> citizens = br
                .lines()
                .skip(0)
                .map(line -> {
                    String[] attributes = line.split(",");
                    return createWallet(attributes);
                })
                .collect(Collectors.toList());
        return citizens;
    }


}
