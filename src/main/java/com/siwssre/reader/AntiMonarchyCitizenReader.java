package com.siwssre.reader;


import java.io.BufferedReader;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class AntiMonarchyCitizenReader implements BaseReader<BufferedReader, Set<String>> {

    /**
     * File reader will return a HashSet<String> based on the content of the file provided as input as BufferedReader
     * @param br
     * @return
     */
    @Override
    public HashSet<String> read(BufferedReader br) {
        return br.lines()
                .collect(Collectors.toCollection(HashSet::new));
    }

}
