package com.siwssre.reader;

import java.io.FileNotFoundException;

public interface BaseReader<I, O> {

    /**
     * Generic read interface to be implemented by any additional reader
     * @param i generic object on your choice based on the implementation
     * @return generic object of your choice based on the implementation
     * @throws FileNotFoundException
     */
    O read(I i) throws FileNotFoundException;
}
