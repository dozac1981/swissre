package com.siwssre.model;

import java.time.LocalDate;

public class Citizen {
    private final String lastName;
    private final String firstName;
    private final LocalDate dob;
    private final String email;


    public Citizen(String lastName, String firstName, LocalDate dob, String email) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.dob = dob;
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public LocalDate getDob() {
        return dob;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Citizen{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", dob=" + dob +
                ", email='" + email + '\'' +
                '}';
    }
}
