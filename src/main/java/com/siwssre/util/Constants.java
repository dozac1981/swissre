package com.siwssre.util;

public class Constants {

    public static final String CITIZENS_TXT = "citizens.txt";
    public static final String ANTI_MONARCHY_CITIZENS_TXT = "antiMonarchyCitizens.txt";
    public static final int MAX_NO_CITIZEN = 20;

    public static final String CPU_ASYNC_EXECUTOR = "cpuAsyncExecutor";
    public static final int CPU_EXECUTOR_SIZE = 16;

}
