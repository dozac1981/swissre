package com.siwssre.util;

import com.siwssre.exception.CustomException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class Utils {

    /**
     * Adding n days to a given LocalDate by skipping weekends adn return the date.
     * @param date
     * @param days
     * @return LocalDate
     */
    public static LocalDate addDaysSkippingWeekends(final LocalDate date, final int days) {
        LocalDate result = date;
        int addedDays = 0;
        while (addedDays < days) {
            result = result.plusDays(1);
            if (!(result.getDayOfWeek() == DayOfWeek.SATURDAY || result.getDayOfWeek() == DayOfWeek.SUNDAY)) {
                ++addedDays;
            }
        }
        return result;
    }

    /**
     * parse and format string date to LocalDate
     * @param dob
     * @return LocalDate
     */
    public static LocalDate formatDob(final String dob) {
        if(dob==null || dob.isEmpty()){
            throw new CustomException("Failed while trying to parse the date of birth", new NullPointerException());
        }
        LocalDate date = LocalDate.parse(dob, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        return date;
    }

    /**
     * Compare two LocalDate obj and check if the day and month are equals
     * @param currDate
     * @param bday
     * @return boolean
     */
    public static boolean isBirthday(final LocalDate currDate, final LocalDate bday) {
        final int date = currDate.getDayOfMonth();
        final Month month = currDate.getMonth();
        return date == bday.getDayOfMonth() && month == bday.getMonth();
    }


    /**
     * Return a BufferedReader obj with a given file as input
     * @param fileName
     * @return BufferedReader
     */
    public static BufferedReader getBufferedReader(final String fileName) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(fileName);
        InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
        return new BufferedReader(streamReader);
    }


}
