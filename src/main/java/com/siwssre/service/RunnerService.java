package com.siwssre.service;

import com.siwssre.config.ThreadUtils;
import com.siwssre.model.Citizen;
import com.siwssre.processor.CitizenProcessor;
import com.siwssre.reader.AllCitizenReader;
import com.siwssre.reader.AntiMonarchyCitizenReader;
import com.siwssre.util.Utils;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.List;

import static com.siwssre.util.Constants.*;

public class RunnerService {

    /**
     * This method kicks off the Readers for reading the citizens List and the excluded emailing list as HashSet passing the data
     * for further asyncronous processing to the underline CitizenService @see {@link CitizenService#process(LocalDate, List, HashSet)}
     */
    public void run() {
        //Read file and return the list of citizens
        AllCitizenReader fl = new AllCitizenReader();
        List<Citizen> citizens = fl.read(Utils.getBufferedReader(CITIZENS_TXT));

        //Read file and return the citizens which do not support the monarchy
        AntiMonarchyCitizenReader antiMonarchyCitizenReader = new AntiMonarchyCitizenReader();
        HashSet<String> antiMonCitizen = antiMonarchyCitizenReader.read(Utils.getBufferedReader(ANTI_MONARCHY_CITIZENS_TXT));

        //Process Citizens asynchronously
        CitizenService rd = new CitizenService(ThreadUtils.createFixedSizeExecutor(CPU_ASYNC_EXECUTOR, CPU_EXECUTOR_SIZE), new CitizenProcessor());
        AbstractMap.SimpleEntry<List<Citizen>, List<Citizen>> results = rd.process(LocalDate.now(), citizens, antiMonCitizen);

        results.getKey().forEach(System.out::println);

        if (results.getValue().size() >= MAX_NO_CITIZEN) {
            results.getValue().forEach(System.out::println);
        }

        System.exit(1);
    }


}
