package com.siwssre.service;

import com.siwssre.exception.CustomException;
import com.siwssre.model.Citizen;
import com.siwssre.processor.CitizenProcessor;
import com.siwssre.util.Utils;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;


public class CitizenService {

    public static final int FIVE_DAYS = 5;
    public static final int TEN_DAYS = 10;
    private final ExecutorService executor;
    private final CitizenProcessor citizenProcessor;

    public CitizenService(ExecutorService executor, CitizenProcessor citizenProcessor) {
        this.executor = executor;
        this.citizenProcessor = citizenProcessor;
    }

    /**
     * Handle the asyncronous process of the citizens list. Rather than processing the list for one date (in a syncronous fashion)
     * this allows to run the processor on multiple threads, by passing in the Lists<> and LocalDate
     * @param batchRunDate
     * @param citizens
     * @param antiMonarchyCitizens
     * @return AbstractMap.SimpleEntry - Ideally a Tuple or Pair should have been used on the return
     * but since the use of other libraries (except Java) is not allowed this was used as alternative.
     */
    public AbstractMap.SimpleEntry<List<Citizen>, List<Citizen>> process(final LocalDate batchRunDate, final List<Citizen> citizens, final HashSet<String> antiMonarchyCitizens) {
            //Calculate the days in advance as of the LocalDate, hence batchRunDate
            LocalDate fiveWeekDaysInAdvance = Utils.addDaysSkippingWeekends(batchRunDate, FIVE_DAYS);
            LocalDate tenWeekDaysInAdvance = Utils.addDaysSkippingWeekends(batchRunDate, TEN_DAYS);

        try {
            // Kick of multiple, asynchronous processing for five and ten week days in advance
            CompletableFuture<List<Citizen>> fiveWeekDays = CompletableFuture.supplyAsync(()
                    -> citizenProcessor.processCitizens(citizens, antiMonarchyCitizens, fiveWeekDaysInAdvance), executor);
            CompletableFuture<List<Citizen>> tenWeekDays = CompletableFuture.supplyAsync(()
                    -> citizenProcessor.processCitizens(citizens, antiMonarchyCitizens, tenWeekDaysInAdvance), executor);
            // Wait until they are all done
            CompletableFuture.allOf(fiveWeekDays, tenWeekDays).join();

            return new AbstractMap.SimpleEntry<>(fiveWeekDays.get(), tenWeekDays.get());

        } catch (InterruptedException | ExecutionException e) {
            throw new CustomException("Error during price calculation and discovery.", e);
        }


    }


}

