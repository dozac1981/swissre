package com.siwssre;


import com.siwssre.service.RunnerService;

public class Main {

    public static void main(String[] args) {

        RunnerService runnerService = new RunnerService();
        runnerService.run();
    }

}
