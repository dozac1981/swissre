package com.siwssre.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public class ThreadUtils {

    public static final int ONE = 1;

    private ThreadUtils() {
    }

    /**
     * The newFixedThreadPool() method of Executors class creates a thread pool that reuses a fixed number of threads.
     * At any point, at most n Threads will be active processing tasks. If additional tasks are submitted when all threads are active,
     * they will wait in the queue until one thread becomes available.
     * @param threadPoolName
     * @param parallelism
     * @return
     */
    public static ExecutorService createFixedSizeExecutor(String threadPoolName, int parallelism) {
        return Executors.newFixedThreadPool(parallelism, createThreadFactory(threadPoolName));
    }

    private static ThreadFactory createThreadFactory(String threadPoolName) {
        return new ThreadFactory() {
            AtomicLong threadValue = new AtomicLong(ONE);

            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setName(threadPoolName + "-" + threadValue.getAndIncrement());
                thread.setDaemon(true);
                return thread;
            }
        };

    }
}
