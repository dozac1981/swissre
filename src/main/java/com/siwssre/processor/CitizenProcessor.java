package com.siwssre.processor;

import com.siwssre.model.Citizen;
import com.siwssre.util.Utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CitizenProcessor {

    /**
     * Stream process the list of citizens by applying given filters
     * 1. filter only citizens which bday fall on the day passed as parameter
     * 2. filter only citizens which want to receive an email
     * 3. Handle duplicate emails by not collecting them into the Map
     * @param citizenList
     * @param antiMonarchyCitizens
     * @param advanceDate
     * @return
     */
    public List<Citizen> processCitizens(final List<Citizen> citizenList, final HashSet<String> antiMonarchyCitizens, final LocalDate advanceDate) {

        HashMap<String, Citizen> citizensMap = citizenList.stream()
                .filter(x -> Utils.isBirthday(x.getDob(), advanceDate)) //filter only citizens which bday fall on the day passed as parameter
                .filter(x -> !antiMonarchyCitizens.contains(x.getEmail())) //filter only citizens which want to receive an email
                .collect(Collectors.toMap(Citizen::getEmail, Function.identity(), (e1, e2) -> null, HashMap::new)); //take care of duplicate emails

        return new ArrayList<>(citizensMap.values());

    }
}
