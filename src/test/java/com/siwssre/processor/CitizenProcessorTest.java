package com.siwssre.processor;

import com.siwssre.model.Citizen;
import com.siwssre.reader.AllCitizenReader;
import com.siwssre.reader.AntiMonarchyCitizenReader;
import com.siwssre.util.Utils;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CitizenProcessorTest {

    AllCitizenReader allCitizenReader = new AllCitizenReader();
    AntiMonarchyCitizenReader antiMonarchyCitizenReader = new AntiMonarchyCitizenReader();
    CitizenProcessor citizenProcessor = new CitizenProcessor();

    @Test
    void test_processCitizens_with_no_edge_case() {
        List<Citizen> citizenList = allCitizenReader.read(Utils.getBufferedReader("text_citizens.txt"));
        HashSet<String> antiMonarchyCitizens = antiMonarchyCitizenReader.read(Utils.getBufferedReader("text_antiMonarchyCitizens.txt"));
        List<Citizen> processedCitizenList = citizenProcessor.processCitizens(citizenList,antiMonarchyCitizens, LocalDate.of(2021, 10, 07));
        assertEquals(1, processedCitizenList.size(), "one citizen is found having birthday on 07/10 based on the the given input");
    }

    @Test
    void test_processCitizens_where_duplicate_email_and_citizen_who_dont_want_email_from_King_are_removed() {
        List<Citizen> citizenList = allCitizenReader.read(Utils.getBufferedReader("testfilesSetTwo/text_citizens.txt"));
        HashSet<String> antiMonarchyCitizens = antiMonarchyCitizenReader.read(Utils.getBufferedReader("testfilesSetTwo/text_antiMonarchyCitizens.txt"));
        List<Citizen> processedCitizenList = citizenProcessor.processCitizens(citizenList,antiMonarchyCitizens, LocalDate.of(2021, 10, 14));
        assertEquals(11, processedCitizenList.size(),
                "based on the given input text files, eleven citizens are found having birthday on 14/10 based, while those with duplicates emails are removed, " +
                        "including the ones which are on the exclusion list, not wanting to receive King's email");
    }
}
