package com.siwssre.util;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.time.LocalDate;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UtilsTest {


    @Test
    void addDaysSkippingWeekends() {
        LocalDate fiveWeekDaysInAdvance = Utils.addDaysSkippingWeekends(LocalDate.of(2021, 9, 30), 5);
        LocalDate tenWeekDaysInAdvance = Utils.addDaysSkippingWeekends(LocalDate.of(2021, 9, 30), 10);
        assertTrue(fiveWeekDaysInAdvance.isEqual(LocalDate.of(2021, 10, 07)), "should be 7 weekdays in advance except weekends respect to date passed in");
        assertTrue(tenWeekDaysInAdvance.isEqual(LocalDate.of(2021, 10, 14)), "should be 10 weekdays in advance except weekends respect to date passed in");
    }

    @Test
    void formatDob() {
        LocalDate result = Utils.formatDob("10-11-1950");
        assertTrue(result.isEqual(LocalDate.of(1950, 11, 10)), "String date passed in as param should be parsed and returned as a LocalDate obj");
    }

    @Test
    void test_is_Birthday() {
        boolean result = Utils.isBirthday(LocalDate.of(2021, 10, 07), LocalDate.of(1970, 10, 07));
        assertTrue(result, "Should return true since the person born on 07-10-1970 celebrates its bday each year on October 7th");
    }

    @Test
    void test_is_not_Birthday() {
        boolean result = Utils.isBirthday(LocalDate.of(2021, 11, 07), LocalDate.of(1970, 10, 07));
        assertFalse(result, "Should return false since the month is different between the two dates passed in as parameters");
    }

    @Test
    void getBufferedReader() {
        BufferedReader br = Utils.getBufferedReader("text_input.txt");
        String result = br.lines().collect(Collectors.joining(System.lineSeparator()));
        assertFalse(result.isEmpty(), "Should return false as the string should not be empty. It should contain the text from the input file passed onto the BufferedReader");
    }
}
