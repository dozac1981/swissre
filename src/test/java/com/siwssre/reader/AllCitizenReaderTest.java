package com.siwssre.reader;

import com.siwssre.model.Citizen;
import com.siwssre.util.Utils;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AllCitizenReaderTest {

    AllCitizenReader allCitizenReader = new AllCitizenReader();

    @Test
    void read() {
        List<Citizen> citizenList = allCitizenReader.read(Utils.getBufferedReader("text_citizens.txt"));
        assertEquals(6, citizenList.size(), "The size of the list should be equal to the content of the input file");
    }
}
