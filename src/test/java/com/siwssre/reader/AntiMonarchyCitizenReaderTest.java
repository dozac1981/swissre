package com.siwssre.reader;

import com.siwssre.util.Utils;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AntiMonarchyCitizenReaderTest {

    AntiMonarchyCitizenReader antiMonarchyCitizenReader = new AntiMonarchyCitizenReader();

    @Test
    void read() {

        HashSet<String> citizenList = antiMonarchyCitizenReader.read(Utils.getBufferedReader("text_antiMonarchyCitizens.txt"));
        assertEquals(3, citizenList.size(), "The size of the list should be equal to the content of the input file");
    }
}
