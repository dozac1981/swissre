package com.siwssre.service;

import com.siwssre.config.ThreadUtils;
import com.siwssre.model.Citizen;
import com.siwssre.processor.CitizenProcessor;
import com.siwssre.reader.AllCitizenReader;
import com.siwssre.reader.AntiMonarchyCitizenReader;
import com.siwssre.util.Utils;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;

import static com.siwssre.util.Constants.CPU_ASYNC_EXECUTOR;
import static com.siwssre.util.Constants.CPU_EXECUTOR_SIZE;
import static org.junit.jupiter.api.Assertions.*;


class CitizenServiceTest {

    AllCitizenReader allCitizenReader = new AllCitizenReader();
    AntiMonarchyCitizenReader antiMonarchyCitizenReader = new AntiMonarchyCitizenReader();

    private ExecutorService executor = ThreadUtils.createFixedSizeExecutor(CPU_ASYNC_EXECUTOR, CPU_EXECUTOR_SIZE);
    private CitizenProcessor citizenProcessor = new CitizenProcessor();

    CitizenService citizenService = new CitizenService(executor, citizenProcessor);


    @Test
    void test_process_where_five_weekdays_criteria_is_meet() {
        List<Citizen> citizens = allCitizenReader.read(Utils.getBufferedReader("text_citizens.txt"));
        HashSet<String> antiMonarchyCitizens = antiMonarchyCitizenReader.read(Utils.getBufferedReader("text_antiMonarchyCitizens.txt"));
        AbstractMap.SimpleEntry<List<Citizen>, List<Citizen>> result = citizenService.process(LocalDate.of(2021, 9, 30), citizens, antiMonarchyCitizens);
        assertFalse(result.getKey().isEmpty(), "five weekdays notice criteria is meet with the given input");
        assertTrue(result.getValue().isEmpty(), "ten weekdays notice criteria is not meet with the given input");
    }

    @Test
    void test_process_where_duplicate_email_and_citizen_who_dont_want_email_from_King_are_removed() {
        List<Citizen> citizens = allCitizenReader.read(Utils.getBufferedReader("testfilesSetTwo/text_citizens.txt"));
        HashSet<String> antiMonarchyCitizens = antiMonarchyCitizenReader.read(Utils.getBufferedReader("testfilesSetTwo/text_antiMonarchyCitizens.txt"));
        AbstractMap.SimpleEntry<List<Citizen>, List<Citizen>> result = citizenService.process(LocalDate.of(2021, 9, 30), citizens, antiMonarchyCitizens);
        assertTrue(result.getKey().isEmpty(), "five weekdays notice criteria is not meet with the given input");
        assertEquals(11, result.getValue().size(), "ten weekdays notice criteria is not meet with the given input");
    }

    @Test
    void test_process_where_five_and_ten_weekdays_criteria_is_meet() {
        List<Citizen> citizens = allCitizenReader.read(Utils.getBufferedReader("testfilesSetOne/text_citizens.txt"));
        HashSet<String> antiMonarchyCitizens = antiMonarchyCitizenReader.read(Utils.getBufferedReader("testfilesSetOne/text_antiMonarchyCitizens.txt"));
        AbstractMap.SimpleEntry<List<Citizen>, List<Citizen>> result = citizenService.process(LocalDate.of(2021, 9, 30), citizens, antiMonarchyCitizens);
        assertEquals(1, result.getKey().size(), "five weekdays notice criteria is meet with the given input");
        assertEquals(20, result.getValue().size(), "ten weekdays notice criteria is meet with the given input");
    }


}
