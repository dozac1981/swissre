### How to run
The project can be run from the [Main class] (https://bitbucket.org/dozac1981/swissre/src/master/src/main/java/com/siwssre/Main.java)


### Potential issue in the Test Requirements which requires clarification

As part of the requirements the King would like to be notified at least five weekdays in advance (the King never works weekends) and if 
there are "a lot" of people turning 100 years old on a particular day (more than 20 people) the King would like 10 weekdays notice.

Based on the above requirement, the project implementation which I wrote does exactly what has been request. However, there is a slight problem with 
this requirement and this is related to the duplication of notifications sent out to the King on a daily basis for the 5 weekdays and 10 weekdays. 

The 10 weekdays will always send notifications (if the more than 20 people have a bday on the same day), while the 5 weekdays notice will do pretty much the same.
So if we run the batch today, 1st October, the 5 weekdays in advance is Oct 8th, while the 10 weekdays is Oct 15th. As the batch moves forward, since its being run on a daily basis, 
the 5 weekday check will eventually run as well for Oct 15th and notifiy the King again with the same list which was already notified by the 10 weekday check few days prior. 

SOLUTION: To overcome this issue, when the 10 weekday check kicks in and send a notification, we could save the <run date and the list of citizens> on a separate file. 
Then we can have the 5 weekday step, before sending its notification to read from that file and remove from its notification the citizens for which the King was already notified 
by the 10 weekday check. The current impl is not doing that since it was not required but I thought I would mention that just in case 
it was implied that I should handle this scenario as well. 
 

### Constraints:
The following constraints have been met as per the given requirements:

* Make use only of Maven for dependency management, Java and Junit for the for implementation. 

* No other open source libraries have been used.

* Data of the input files found under /resources folder which are being used for testing and running the project was taken 
from the PDF received with the test requirements. For testing the requirements and some of the edge cases, this was replicated for the various scenarios. 
Below is the sample data received.

 - Citizens of Llamaland
```
Brown,Bobby,10-11-1950,bobby.brown@ilovellamaland.com
O'Rourke,Betsy,28-02-1900,betsy@heyitsme.com
Von Tappo,Alfredo,01-01-1920,alfie@vontappo.llama.land
```
 - Citizens do not support the monarchy and have opted out of receiving the mail.
```
betsy@heyitsme.com
randomperson@llama.land
```



